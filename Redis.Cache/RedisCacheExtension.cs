﻿using Microsoft.Extensions.Caching.Distributed;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Redis.Cache
{
    public static class RedisCacheExtension
    {
        public static void Set<TResult>(this IDistributedCache cache, string key, TResult[] values, DistributedCacheEntryOptions options, Expression<Func<TResult, object>> uniquePropertyExpression)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (uniquePropertyExpression == null)
                throw new ArgumentNullException(nameof(uniquePropertyExpression));

            var uniquePropertyName = ((MemberExpression)uniquePropertyExpression.Body).Member.Name;
            var property = typeof(TResult).GetProperty(uniquePropertyName);
            if (!CheckValuePrimitiveType(property.PropertyType))
                throw new TypeAccessException(nameof(uniquePropertyExpression));

            RedisCollectionCache.Cache.KeyDelete(RedisCollectionCache.GetRedisKey(key));

            var defineValues = RedisCollectionCache.GetRedisExpiration(options);


        }

        private static bool CheckValuePrimitiveType(Type type)
        {
            return type.IsValueType || type.IsPrimitive ||
                    new Type[] {
                        typeof(string),
                        typeof(decimal),
                        typeof(DateTime),
                        typeof(DateTimeOffset),
                        typeof(TimeSpan),
                        typeof(Guid)
                    }.Contains(type) ||
                    Convert.GetTypeCode(type) != TypeCode.Object;
        }

        public static TResult[] GetArray<TResult>(this IDistributedCache cache, string key, Expression<Func<TResult, bool>> filter = null)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return cache.GetAndRefreshArray(key, getData: true, filter: filter);
        }

        private static TResult[] GetAndRefreshArray<TResult>(this IDistributedCache cache, string key, bool getData = false, Expression<Func<TResult, bool>> filter = null)
        {
            throw new NotImplementedException();
        }
    }
}
