﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace Redis.Cache
{
    internal class RedisCollectionCache : RedisCache, IDistributedCache
    {
        internal const string SetOptionsFieldsLuaScript = @"
                redis.call('HMSET', KEYS[1], 'absexp', ARGV[1], 'sldexp', ARGV[2])
                if ARGV[3] ~= '-1' then
                    redis.call('EXPIRE', KEYS[1], ARGV[3])
                end
                return 1";

        internal const string SetFieldsLuaScript = "redis.call('HMSET', KEYS[1], {0})";

        internal const int ArgvIndex = 4;

        internal const string FieldCount = "fieldCount";

        protected const string AbsoluteExpirationKey = "absexp";
        protected const string SlidingExpirationKey = "sldexp";
        protected const string UniqueKeys = "keys";
        internal const long NotPresent = -1;
        internal static RedisCacheOptions _redisCacheOptions { get; set; }

        internal static IDatabase Cache
        {
            get
            {
                return Connection.GetDatabase();
            }
        }

        private static readonly Lazy<ConnectionMultiplexer> LazyConnection
          = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(_redisCacheOptions.ConfigurationOptions));

        internal static ConnectionMultiplexer Connection
        {
            get
            {
                return LazyConnection.Value;
            }
        }
        internal RedisCollectionCache(IOptions<RedisCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
            if (optionsAccessor == null || optionsAccessor.Value == null)
                throw new ArgumentNullException(nameof(optionsAccessor));

            if (optionsAccessor.Value.ConfigurationOptions == null || optionsAccessor.Value.ConfigurationOptions.EndPoints == null)
                throw new ArgumentNullException(nameof(optionsAccessor.Value.ConfigurationOptions));

            if (string.IsNullOrEmpty(optionsAccessor.Value.InstanceName))
                throw new ArgumentNullException(nameof(optionsAccessor.Value.InstanceName));

            _redisCacheOptions = optionsAccessor.Value;
        }

        internal static RedisKey GetRedisKey(string key)
        {
            return $"{_redisCacheOptions.InstanceName}_{key}";
        }

        internal static List<RedisValue> GetRedisExpiration(DistributedCacheEntryOptions options)
        {
            var creationTime = DateTimeOffset.UtcNow;

            var absoluteExpiration = GetAbsoluteExpiration(creationTime, options);

            var redisValue = new List<RedisValue>()
            {
                absoluteExpiration?.Ticks ?? NotPresent,
                options.SlidingExpiration?.Ticks ?? NotPresent,
                GetExpirationInSeconds(creationTime, absoluteExpiration, options) ?? NotPresent,
            };

            return redisValue;
        }


        private static DateTimeOffset? GetAbsoluteExpiration(DateTimeOffset creationTime, DistributedCacheEntryOptions options)
        {
            if (options.AbsoluteExpiration.HasValue && options.AbsoluteExpiration <= creationTime)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(DistributedCacheEntryOptions.AbsoluteExpiration),
                    options.AbsoluteExpiration.Value,
                    "The absolute expiration value must be in the future.");
            }
            var absoluteExpiration = options.AbsoluteExpiration;
            if (options.AbsoluteExpirationRelativeToNow.HasValue)
            {
                absoluteExpiration = creationTime + options.AbsoluteExpirationRelativeToNow;
            }

            return absoluteExpiration;
        }

        private static long? GetExpirationInSeconds(DateTimeOffset creationTime, DateTimeOffset? absoluteExpiration, DistributedCacheEntryOptions options)
        {
            if (absoluteExpiration.HasValue && options.SlidingExpiration.HasValue)
            {
                return (long)Math.Min(
                    (absoluteExpiration.Value - creationTime).TotalSeconds,
                    options.SlidingExpiration.Value.TotalSeconds);
            }
            else if (absoluteExpiration.HasValue)
            {
                return (long)(absoluteExpiration.Value - creationTime).TotalSeconds;
            }
            else if (options.SlidingExpiration.HasValue)
            {
                return (long)options.SlidingExpiration.Value.TotalSeconds;
            }
            return null;
        }
    }
}
